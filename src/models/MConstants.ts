export class MConstants{
    public static PATIENT_NODE = 'PATIENTS' ;
    public static EXPENSES_NODE = 'EXPENSES' ;
    public static EXPENSE_TYPE_NODE = 'EXPENSETYPE' ;
    public static NAME = '_name' ;
    public static NEGATE = '~' ;
    public static CREATION_DATE = '_creationDate' ;
    public static USER_NODE = 'USER';
    public static USERID = '_userID' ;
}