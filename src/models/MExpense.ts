export interface DExpense{
  _id?: string,
  _expenseType: string;
  _description: string;
  _amount: number,
  _imageUrl?: string,
  _creationDate?: number,
  _rcreationDate?: number,
  _createdBy?: string
}
export interface SExpense{
  _id?: string,
  _expenseType: string;
  _description: string;
  _amount: number,
  _imageUrl?: string,
  _creationDate?: string
}
export interface SExpenseType{
  _id?: string,
  _expenseType: string;
  _creationDate?: number,
  _rcreationDate?: number,
  _createdBy?: string
}