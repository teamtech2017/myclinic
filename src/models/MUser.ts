export interface MUser{
    _id: string,
    _userID: string,
    _clinicID?: string,
    _name?: string,
    _role?: string
}