export interface MClinic{
  _id:string,
  _name:string,
  _address?: string,
  _logo?: string,
  _creationDate?: string
 
}