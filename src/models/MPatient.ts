export interface DPatient {
    _id?: string,
    _name: string,
    _aadhar?: string,
    _gender: string,
    _dob?: string,
    _age?: number,
    _address?: string,
    _imageUrl?: string,
    _creationDate?: number,
    _rcreationDate?: number,
    _createdBy?: string

}


export interface SPatient {
    _id?: string,
    _name: string,
    _aadhar?: string,
    _gender: string,
    _dob?: string,
    _age?: number,
    _address?: string,
    _imageUrl?: string

}