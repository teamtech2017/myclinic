import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { FirebaseProvider } from '../firebase/firebase';
import { SPatient, DPatient } from '../../Models/MPatient';
import { MConstants } from '../../Models/MConstants';
import { Storage } from '@ionic/storage';
import * as _ from 'lodash';
/*
  Generated class for the PatientsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PPatient {

  _clinicID:string ;

  constructor(public fb: FirebaseProvider,public storage:Storage) {
     this.storage.get('user').then((userObj) => {
      this._clinicID = userObj._clinicID;
       });
  }

  addPatient(_spatient: SPatient) {
    let _dpatient: DPatient = this.mapStoD(_spatient);
    this.fb.saveEntityWithID(_dpatient, MConstants.PATIENT_NODE, _dpatient._id);
  }


  async getPatients(queryString: string): Promise<any> {
    const _value = await  this.fb.executeQueryOnce(MConstants.PATIENT_NODE, MConstants.NAME,queryString, queryString + MConstants.NEGATE, 10)

    return new Promise((resolve, reject) => {
       let _spatients: SPatient[] = [];
          _.forEach(_value, (value) => {
            _spatients.push(this.mapDtoS(value));
          });
          resolve(_spatients);
    }) ;

  }

  async getAllPatients():Promise<any>{
    const _value = await this.fb.getAllEntities(MConstants.PATIENT_NODE) ;

    return new Promise((resolve,reject)=>{
      let _spatients: SPatient[] = [];
          _.forEach(_value, (value) => {
            _spatients.push(this.mapDtoS(value));
          });
          resolve(_spatients);
    }) ;
  }


  mapStoD(_spatient: SPatient): DPatient {
    let _dpatient: DPatient = {
      _id: this.fb.getEntityId(MConstants.PATIENT_NODE),
      _name: _spatient._name,
      _aadhar: _spatient._aadhar,
      _gender: _spatient._gender,
      _dob: _spatient._dob,
      _age: _spatient._age,
      _address: _spatient._address,
      _imageUrl: _spatient._imageUrl
    };
    return _dpatient;

  }

  mapDtoS(_dpatient: DPatient): SPatient {
    let _spatient: SPatient = {
      _id: _dpatient._id,
      _name: _dpatient._name,
      _aadhar: _dpatient._aadhar,
      _gender: _dpatient._gender,
      _dob: _dpatient._dob,
      _age: _dpatient._age,
      _address: _dpatient._address,
      _imageUrl: _dpatient._imageUrl

    };
    return _spatient;
  }


}
