import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { FirebaseProvider } from '../firebase/firebase';
import { SExpense, DExpense,SExpenseType } from '../../models/MExpense';
import { MConstants } from '../../Models/MConstants';
import * as _ from 'lodash';
/*
  Generated class for the ExpensesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PExpenses {

  constructor(public fb: FirebaseProvider) {
    console.log('Hello ExpensesProvider Provider');
  }

  addExpense(_sexpense: SExpense) {
    let _dexpense: DExpense = this.mapStoD(_sexpense);
    this.fb.saveEntityWithID(_dexpense, MConstants.EXPENSES_NODE, _dexpense._id);
  }

  addExpenses(exp: SExpense) {
    console.log(exp);
    let _dexpense: DExpense = this.mapStoD(exp);
    this.fb.saveEntityWithID(_dexpense, MConstants.EXPENSES_NODE, _dexpense._id);
  }

  addExpenseTypes(exp: SExpenseType) {
    console.log(exp);
    exp._id = this.fb.getEntityId(MConstants.EXPENSE_TYPE_NODE);
    this.fb.saveEntityWithID(exp, MConstants.EXPENSE_TYPE_NODE, exp._id);
  }

  removeExpenseType(exp: SExpenseType) {
    console.log(exp);
    exp._id = this.fb.getEntityId(MConstants.EXPENSE_TYPE_NODE);
    this.fb.saveEntityWithID(exp, MConstants.EXPENSE_TYPE_NODE, exp._id);
  }

  async getAllExpenses() {
    console.log("getAllExpenses");
   // let ref= firebase.database().ref().child('expense');
    //let fbref = firebase.database().ref();
   // const _value = await  this.fb.executeQueryOnce(MConstants.EXPENSES_NODE, MConstants.NAME,queryString, queryString + MConstants.NEGATE, 10)
    var expList = this.fb.getAllEntities(MConstants.EXPENSES_NODE);
    console.log("expList : "+expList);
    return expList;
  }

async getExpenseTypes(): Promise<any> {
  console.log("getExpenseTypes");
    const _value = await  this.fb.executeQuery(MConstants.EXPENSE_TYPE_NODE, MConstants.CREATION_DATE, 10);
    console.log("ExpenseTypes : "+_value.length);
    return new Promise((resolve, reject) => {
       let _sexpenses: SExpenseType[] = [];
            _.forEach(_value, (value) => {
            _sexpenses.push(value);
          });
          resolve(_sexpenses);
    }) ;
  }

async getExpenses(): Promise<any> {
  console.log("getExpenses");
    const _value = await  this.fb.executeQuery(MConstants.EXPENSES_NODE, MConstants.CREATION_DATE, 10);
    console.log("Expenses : "+_value.length);
    return new Promise((resolve, reject) => {
       let _sexpenses: SExpense[] = [];
            _.forEach(_value, (value) => {
            _sexpenses.push(this.mapDtoS(value));
          });
          resolve(_sexpenses);
    }) ;

  }  
mapStoD(_sexpense: SExpense): DExpense {
    let _dexpense: DExpense = {
      _id: this.fb.getEntityId(MConstants.EXPENSES_NODE),
      _expenseType: _sexpense._expenseType,
      _description: _sexpense._description,
      _amount: _sexpense._amount,
      _imageUrl: _sexpense._imageUrl
    };
    return _dexpense;

  }

  mapDtoS(_dexpense: DExpense): SExpense {
    let _sexpense: SExpense = {
      _id: _dexpense._id,
      _expenseType: _dexpense._expenseType,
      _description: _dexpense._description,
      _amount: _dexpense._amount,
      _imageUrl: _dexpense._imageUrl,
      _creationDate: new Date(_dexpense._creationDate)+""
    };
    return _sexpense;
  }

}
