import { Injectable } from '@angular/core';
import { LoadingController, ToastController, AlertController } from 'ionic-angular';
import 'rxjs/add/operator/map';

/*
  Generated class for the UtilProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UtilProvider {
 public loader: any;
    constructor(private loadingCtrl: LoadingController, 
                private toastCtrl: ToastController, 
                private alertCtrl: AlertController) {

    }

    presentLoader() {
        this.loader = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Please wait ...'
        });
        this.loader.present();
    }

    dismissLoader() {
        this.loader.dismiss();
    }

    presentAlert(msg:string) {
        let alert = this.alertCtrl.create({
            title: 'Information',
            message: msg,
            buttons: ['Dismiss']
        });
        alert.present();
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'center'
        });
        toast.present();
    }

    debug(logMsg: String){
        console.log(logMsg) ;
    }

    getObject(map){
        Object.keys(map).forEach((key)=> {
            return map[key]
        }) ;
    }

}
