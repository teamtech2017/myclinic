import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { MUser } from '../../models/MUser';
import firebase from 'firebase';
import * as _ from 'lodash';
import { Storage } from '@ionic/storage';

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class FirebaseProvider {

  public fb: any;
  private user: MUser;
  private _clinicID:any;

  constructor(public storage: Storage) {
    this.storage.get('user').then((userObj) => {
        this.user=userObj;
        this._clinicID = userObj._clinicID;
        console.log('ClinincID:'+this._clinicID+', User:'+this.user._userID);
       });
    
    console.log('Hello FirebaseProvider Provider');
  }

  init() {
    var config = {
      apiKey: "AIzaSyCK9V0PgF0aGQ2ZycyGxtcYMcNC9Phaf8M",
      authDomain: "myclinic-49358.firebaseapp.com",
      databaseURL: "https://myclinic-49358.firebaseio.com",
      projectId: "myclinic-49358",
      storageBucket: "myclinic-49358.appspot.com",
      messagingSenderId: "400693943719"
    };
    firebase.initializeApp(config);

    this.fb = firebase.database();
    console.log("firebase initialized " + this.fb);
  }

  saveEntityWithID(entity: any, childNodeName: string, _id: string) {
    entity._createdBy=this.user._id;
    entity._creationDate= new Date().getTime();
    entity._rcreationDate= -1.0 * new Date().getTime();
    firebase.database().ref().child(this.appendRoot(childNodeName) + "/" + _id).set(entity);
  }

  appendRoot(childNodeName: string): string {
    return this._clinicID+"/"+childNodeName;
  }

  saveEntity(entity: any, childNodeName: string) {
    firebase.database().ref().child(childNodeName).push(entity);
  }

  getEntityId(childNodeName: string): string {
    return firebase.database().ref().child(this.appendRoot(childNodeName)).push().key;
  }

  getEntityIdfromRoot() {
    return firebase.database().ref().push().key;
  }

  executeQueryOnce(childNodeName: string, orderByChild: string, startAt: string,
    endAt: string, limitToFirst: number): Promise<any> {

    return new Promise((resolve, reject) => {
      childNodeName=this.appendRoot(childNodeName);
      firebase.database().ref().child(childNodeName).orderByChild(orderByChild)
        .startAt(startAt).endAt(endAt).limitToFirst(limitToFirst).once('value', (snapshot) => {
          console.log(snapshot.val());
          resolve(_.values(snapshot.val()))
        })
    });
  }

  executeQuery(childNodeName: string, orderByChild: string, limitToFirst: number): Promise<any> {
    console.log("executeQuery ... childNodeName:" + childNodeName +", orderByChild"+orderByChild+", limitToFirst"+limitToFirst);
    return new Promise((resolve, reject) => {
      childNodeName=this.appendRoot(childNodeName);
      firebase.database().ref().child(childNodeName).orderByChild(orderByChild)
        .limitToFirst(limitToFirst).once('value', (snapshot) => {
          console.log(snapshot.val());
          resolve(_.values(snapshot.val()))
        })
    });
  }

  getAllEntities(childNodeName: string) {
    return new Promise((resolve, reject) => {
      firebase.database().ref().child(this.appendRoot(childNodeName)).once('value', (snapshot) => {
        resolve(_.values(snapshot.val()))
      })
    });
  }

  getEntityByName(childNodeName: string, orderByChild: string, equalTo: string) {
    return new Promise((resolve, reject) => {
      firebase.database().ref().child(childNodeName).orderByChild(orderByChild).equalTo(equalTo).once('value', (snapshot) => {

        resolve(_.values(snapshot.val())[0]);
      })
    });
  }

  getUser(email: string, password: string) {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  registerUser(email: string, password: string) {
    return firebase.auth().createUserWithEmailAndPassword(email, password);
  }

  setLoggedInUser(user: MUser) {
    this.user = user;
  }

  getLoggedInUser() {
    return this.user;
  }

}
