import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, MenuController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { FirebaseProvider } from '../../providers/firebase/firebase';
import { TabsPage } from '../tabs/tabs' ;

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
public loginForm: any;
  constructor(public nav: NavController, public formBuilder: FormBuilder,
    public menu: MenuController, public fbService: FirebaseProvider,
    public storage: Storage,  public events:Events) {
    // disable menu

    this.menu.swipeEnable(false);
    this.loginForm = formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

   loginUser(event) {
     this.fbService.getUser(this.loginForm.value.email,this.loginForm.value.password).then((user)=>{
       //console.log(user) ;
        this.fbService.getEntityByName('USER','_userID',user.email).then((response)=>{
          this.storage.set('user', response);
          this.events.publish('LoggedIn', response) ;
          this.nav.setRoot(TabsPage) ;
        })
     }).catch((error)=>{
       console.log(error) ;
       this.nav.setRoot('LoginPage') ;
     })

     
   }

  
}
