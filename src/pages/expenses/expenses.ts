import { Component,ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UtilProvider } from '../../providers/util/util';
import { SExpense } from '../../models/MExpense';
import { PExpenses } from '../../providers/expenses/PExpenses';

/**
 * Generated class for the ExpensesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-expenses',
  templateUrl: 'expenses.html',
})
export class ExpensesPage {
  ExpensesTabs : string='List';
  @ViewChild('barCanvas') barCanvas;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  @ViewChild('lineCanvas') lineCanvas;
  expense : any;
  expensesList= [];
  public postForm: any;
   // lineCanvas:any;
 
    barChart: any;
    doughnutChart: any;
    lineChart: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public expProvider: PExpenses, public formBuilder: FormBuilder, public util: UtilProvider) {
      this.postForm = formBuilder.group({
          expType: ['', Validators.required],
          description: ['', Validators.required],
          amount: ['', Validators.required]
      });this.loadExpenses();
  }

  toggleSegment(){
   // console.log(this.ExpensesTabs) ;
   this.loadExpenses();
   this.loadCharts() ;
  }
  
  ionViewDidLoad() {
        console.log('ionViewDidLoad ExpensesPage');
        //alert(this.barCanvas);
      
  }     

  async saveExpense(event) {
    if ((this.postForm.value.expType == undefined || this.postForm.value.expType == '') &&
    (this.postForm.value.description == undefined || this.postForm.value.description == '') &&
    (this.postForm.value.amount == undefined || this.postForm.value.amount == '') ) {
      this.util.presentAlert('Please Provide Necessary Information ');
    } else {
      this.util.presentLoader();
      let exp: SExpense = {
        _expenseType: this.postForm.value.expType,
        _description: this.postForm.value.description ,
        _amount: this.postForm.value.amount,
        _imageUrl: ''
      }
      this.expProvider.addExpenses(exp);
      this.postForm.reset() ;
      this.util.dismissLoader();
      this.navCtrl.setRoot('ExpensesPage') ;
    }
  }

loadExpenses(){
    if(this.ExpensesTabs == 'List'){
        this.expProvider.getExpenses().then((value)=> {
            this.expensesList=value;
          });
        
    }
}

loadCharts(){
    if(this.ExpensesTabs == 'Reports'){
      this.barChart = new Chart(this.barCanvas.nativeElement, {
 
            type: 'bar',
            data: {
                labels: ["Income", "Expenses"],
                datasets: [{
                    label: 'Income vs Expenses',
                    data: [35000, 19000],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
 
        });
        this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
 
            type: 'doughnut',
            data: {
                labels: ["Salaries", "Dental Depo", "Instruments", "Stationary", "Rental", "Misc"],
                datasets: [{
                    label: 'Expenses by Types',
                    data: [5200, 9000, 3000, 500, 7000, 1500],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }]
            }
 
        });
 
        this.lineChart = new Chart(this.lineCanvas.nativeElement, {
 
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "Income",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(255, 99, 132, 0.2)",
                        borderColor: "rgba(245, 0, 98, 1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [6500, 5900, 8000, 8100, 5600, 5500, 4000],
                        spanGaps: false,
                    },
                    {
                        label: "Expenses",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(205, 255, 97, 1)",
                        borderColor: "rgba(8, 245, 0, 1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(54, 162, 235, 0.2)",
                        pointBackgroundColor: "#fffDD",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [5500, 5000, 4000, 3100, 3600, 750, 2000],
                        spanGaps: false,
                    }
                ]
            }
 
        });
    }
   } 
}