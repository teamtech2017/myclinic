import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';

import { SPatient } from '../../Models/MPatient';
import { PPatient } from '../../providers/patients/PPatient';

import * as _ from 'lodash';
import * as moment from 'moment';

/**
 * Generated class for the PatientsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-patients',
  templateUrl: 'patients.html',
})
export class PatientsPage {
  @ViewChild('fileInput') fileInput;
  currentPatients: any = [];
  patientsTabs: string = 'List';
  patientTemp: SPatient;
  patientsList: any = [];

  patientTempTab: string = 'info' ;
  

  public patientForm: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public patientService: PPatient) {
    this.patientForm = this.formBuilder.group({
      name: ['', Validators.required],
      aadhar: ['', Validators.required],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      address: [''],
      imageUrl: ['']
    });
    if (this.patientsList.length == 0) {
      this.patientService.getAllPatients().then((response) => {
        this.patientsList = response;
        this.currentPatients=response ;
      });
    }else{
      this.currentPatients = this.patientsList ;
    }


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientsPage');
  }

  addPatient(ev) {
    //console.log(this.patientForm.value) ;
    let _spatient: SPatient = {
      _id: '',
      _name: this.patientForm.value.name.toUpperCase(),
      _aadhar: this.patientForm.value.aadhar,
      _gender: this.patientForm.value.gender,
      _dob: this.patientForm.value.dob,
      _age: moment().diff(this.patientForm.value.dob, 'years'),
      _address: this.patientForm.value.address,
      _imageUrl: ''

    };

    this.patientService.addPatient(_spatient);
    this.patientForm.reset();
  }

  getPatients(ev) {
    let val = ev.target.value;
    this.currentPatients = [];
    if (!val || !val.trim()) {
      this.patientTemp = undefined;
      this.currentPatients = this.patientsList ;
      return;
    }

    this.patientService.getPatients(val.toUpperCase()).then((response) => {
      this.currentPatients = [];
      this.patientTemp = undefined;
      this.currentPatients = response;
      console.log(this.currentPatients);
    });
  }

  getPatientInfo(id) {
    this.patientTemp = _.find(this.currentPatients, { '_id': id });
  }

 

  toggleSegment(){
    if(this.patientsTabs == 'List'){
      this.currentPatients = this.patientsList ;
      this.patientTemp = undefined ;
    }
  }

  togglePatientInfoSegment(){

  }

}
