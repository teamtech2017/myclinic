import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentsPage } from './appointments';

import { NgCalendarModule  } from 'ionic2-calendar';

@NgModule({
  declarations: [
    AppointmentsPage,
  ],
  imports: [
    NgCalendarModule,
    IonicPageModule.forChild(AppointmentsPage),
  ],
  exports: [
    AppointmentsPage
  ]
})
export class AppointmentsPageModule {}
