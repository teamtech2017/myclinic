import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ThemeProvider } from '../../providers/theme/theme';


/**
 * Generated class for the SettingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  selectedTheme: String;
  selectOptions: any ;
  

  constructor(public navCtrl: NavController, public navParams: NavParams,public tp: ThemeProvider) {
     this.tp.getActiveTheme().subscribe(val => this.selectedTheme = val);
     this.selectOptions = {
      cssClass: this.selectedTheme
    } ;
  }

  onCancel(){
    console.log('cancel') ;
  }

 onChange(value){
  this.tp.setActiveTheme(value) ;
   this.selectOptions = {
      cssClass: this.selectedTheme
    } ;
}
showExpenseTypes(){
  this.navCtrl.setRoot('ExpensetypePage');
}
  

}
