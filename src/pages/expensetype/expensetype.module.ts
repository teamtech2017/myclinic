import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExpensetypePage } from './expensetype';

@NgModule({
  declarations: [
    ExpensetypePage,
  ],
  imports: [
    IonicPageModule.forChild(ExpensetypePage),
  ],
  exports: [
    ExpensetypePage
  ]
})
export class ExpensetypePageModule {}
