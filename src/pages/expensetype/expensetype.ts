import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SExpenseType } from '../../models/MExpense';
import { PExpenses } from '../../providers/expenses/PExpenses';
import { FormBuilder, Validators } from '@angular/forms';
import { UtilProvider } from '../../providers/util/util';

/**
 * Generated class for the ExpensetypePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-expensetype',
  templateUrl: 'expensetype.html',
})
export class ExpensetypePage {
  ExpenseTypesTabs : string='List';
  expensesList= [];
  public postForm: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public expProvider: PExpenses,public formBuilder: FormBuilder,public util: UtilProvider) {
  this.postForm = formBuilder.group({
          expType: ['', Validators.required]
      });
      this.loadExpenseTypes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExpensetypePage');
  }

  addExpenseType(){
    this.navCtrl.setRoot('SettingsPage');
  }
  toggleSegment(){
    if(this.ExpenseTypesTabs == 'List'){
        this.loadExpenseTypes();
    }
    if(this.ExpenseTypesTabs == 'Back'){
        this.navCtrl.setRoot('SettingsPage');
    }
   console.log(this.ExpenseTypesTabs) ;
   
  }
  loadExpenseTypes(){
    this.expProvider.getExpenseTypes().then((value)=> {
            this.expensesList=value;
          });
  }

  async saveExpenseTypes(event) {
    if ((this.postForm.value.expType == undefined || this.postForm.value.expType == '')) {
      this.util.presentAlert('Please Provide Necessary Information ');
    } else {
      this.util.presentLoader();
      let exp: SExpenseType = {
        _expenseType: this.postForm.value.expType
      }
      this.expProvider.addExpenseTypes(exp);
      this.postForm.reset() ;
      this.util.dismissLoader();
      this.navCtrl.setRoot('ExpensetypePage') ;
    }
  }

  async removeExpenseType(event) {
      console.log(event) ;
    
      /*this.util.presentLoader();
      let exp: SExpenseType = {
        _expenseType: event.expType;
      }
      this.expProvider.addExpenseTypes(exp);
      this.postForm.reset() ;
      this.util.dismissLoader();
      this.navCtrl.setRoot('ExpensetypePage') ;
    }*/
  }

}
