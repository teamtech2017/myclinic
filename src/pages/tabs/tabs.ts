import { Component } from '@angular/core';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'AppointmentsPage';
  tab2Root = 'PatientsPage';
  tab3Root = 'ExpensesPage';
  tab4Root = 'SettingsPage' ;

  constructor() {

  }
}
