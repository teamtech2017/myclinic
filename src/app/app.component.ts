import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';

import { FirebaseProvider } from '../providers/firebase/firebase';
import { ThemeProvider } from '../providers/theme/theme';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  isLoggedInUser = false;
  rootPage: any = TabsPage;
  public user: any;
  selectedTheme: String;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    public events: Events, public storage: Storage, public fb: FirebaseProvider, public tp: ThemeProvider) {
    this.tp.getActiveTheme().subscribe(val => this.selectedTheme = val);
    this.storage.get('user').then((data) => {
      this.user = data;
      if (this.user != undefined) {
        this.isLoggedInUser = true
        this.rootPage = TabsPage;
      } else {
        this.rootPage = 'LoginPage';
      }
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.fb.init();
      this.subscribeEvents();
    });
  }

  subscribeEvents() {

    this.events.subscribe('LoggedIn', (data) => {
      console.log("loggedin" + JSON.stringify(data));

      this.user = data;
      this.isLoggedInUser = true

    });
    this.events.subscribe('LoggedOut', (data) => {
      this.user = null;
      this.isLoggedInUser = false;
    });
  }



}
